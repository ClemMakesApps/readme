# Clement Ho - Manager README

## Purpose of this document

There are a lot of conversations about manager READMEs. Some people advocate for it, while others wholeheartedly disagree with them. Both have legitimate reasons but here are my reasons for writing this document:

- I'd like to create a space where prospective candidates can learn about my management style to see whether they could picture themselves working for my team. Every manager is different. If I were evaluating a position, it would be valuable to learn about my future manager before committing to be a part of their team.
- It's easy to say you are going to do something but difficult to act on it. This document is also meant to be a tool of accountability. In some regard, it is similar to a declaration of what I pledge to do as a manager.

## How to be successful on my team

- Ownership
  - Effective team members take ownership of the tasks that are assigned to them and help drive them to completion. If they need help, they do not hesitate to seek for help so that they can complete their work.

- Communication
  - Effective team members communicate clearly with everyone around them. 

- Collaboration
  - Effective team members collaborate well with everyone around them. They are also willing to lend a hand whenever someone else needs help.

- Receives and gives feedback well
  - Effective team members receive and give feedback well. This helps them continually improve to become better team members as they learn and grow.

## What you should expect from me 

- I always want to have a Posture of learning
  - I will need help and feedback to figure out whether what I am doing is the best way forward. Each team and each team member is different, and so it's important to figure out which parts are uncomfortable because it's different or whether it is something that can be improved. Iterating through ideas and learning from mistakes is in my opinion, the best way to become a better manager.

- Team's success is my success
  - I do my best to help my team succeed. I try my best to ensure that my team members know where they are in their career growth, challenge them to keep growing and help resolve conflicts whenever they need my assistance. I also do my best to unblock my team members whenever they are blocked from doing their work.

- I care about my team's well being
  - As someone who's worked remotely for 2+ years, I've found wellness and work-life balance to be the most challenging since there isn't as much accountability as there is in a physical office. I strongly discourage my team to work on weekends or after hours and offer to hold them accountable if they wish me to do so. Some team members like having a non-typical work day, in these cases, I try to send a friendly message just in case because I'd rather be proactive in maintaining my team's work-life balance, rather than seeing them burnout.

## What my team does differently from the typical team

### Walking 1:1's

In light of trying to improve wellness in the team since we sit all day and to create space for less formal/structured conversation, if my team member would like to participate, I schedule a walking 1:1 every other week with team members after they’ve joined for more than 90 days. New team members typically have more questions that require screen share, so it is more efficient to do walking meetings after team members have adapted to the team.

Walking 1:1’s is where we do an audio call and are encouraged to go out for a walk while we go through our typical 1:1 topics. I also encourage to use that time to pick each others brains on different topics such as how to improve our team or discuss how we are doing well. So far, it has been well received by the team.

### Testimonials
"*I really enjoy the walking 1:1's, I think it's a great way to encourage me to get up and stretch my legs after sitting in front of the computer the entire day!*" - Jacques Erasmus

### Quarterly personal goals

In my opinion, if we don't set goals, we are less likely to do them. Similar to how we do OKR's at GitLab, I like to do quarterly personal goals with my team members. These are typically geared towards career growth or ways to become more effective in their role. Sometimes they map to OKRs set by the team and sometimes they are focused on honing in a skillset that demonstrates the next level requirement (Eg. finding ways to demonstrate architectural perspective). I try to make the goals a little stretching, aiming for 70% completion, so that we use it as a strategy for stretching ourselves rather than just a checklist to complete.
